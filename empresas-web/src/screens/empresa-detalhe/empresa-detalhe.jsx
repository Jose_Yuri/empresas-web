import React from 'react';
import Empresa from '../../Assets/img-e-1-lista.png'
import './empresa-detalhe.css'
import { useHistory } from 'react-router-dom'


const Detalhe = (props) => {
    const { location } = props
    const routerControl = useHistory()
    return (
        <div className="Detalhe col s12 m7">
            <nav>
                <div className="nav-wrapper valign-wrapper">
                    <ul className="left ">
                        <li> <a onClick={routerControl.goBack}>   <i className="material-icons ">arrow_back</i>  </a> </li>
                        <li> <a href="#" className="a">  {location.state.empresa.enterprise_name}  </a> </li>
                    </ul>
                </div>
            </nav>
            <div>
            <div id="cardDetalhe" className="card">
                <div className="card-content"> 
                    <img  src={Empresa} />
                    <h3 > {location.state.empresa.description} </h3>
                </div>
            </div>
        </div>
        </div>
    )
}

export default Detalhe