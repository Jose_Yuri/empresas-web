import React, { useState } from 'react';
import { useHistory } from 'react-router-dom'
import "./login-style.css"
import "materialize-css/dist/css/materialize.min.css";
import Logo from "../../Assets/logo-home.png"
import Logo2x from '../../Assets/logo-home@2x.png'
import Logo3x from '../../Assets/logo-home@3x.png'
import MailIcon from '../../Assets/ic-email.png'
import MailIcon2x from '../../Assets/ic-email@2x.png'
import MailIcon3x from '../../Assets/ic-email@3x.png'
import LockIcon from '../../Assets/ic-cadeado.png'
import LockIcon2x from '../../Assets/ic-cadeado@2x.png'
import LockIcon3x from '../../Assets/ic-cadeado@3x.png'
import axios from 'axios'

const Login = () => {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const routeHook = useHistory();

    const handleLogin = async (e) => {
        
        e.preventDefault()
        const response = await axios.post('https://empresas.ioasys.com.br/api/v1/users/auth/sign_in', { email: email, password: password })
        console.log(response.data)
        console.log(response.headers)
        localStorage.setItem('access-token', response.headers['access-token'])
        localStorage.setItem('client', response.headers['client'])
        localStorage.setItem('uid', response.headers['uid'])
        routeHook.push("/home")

    }

    return (
        <div className="_Login">
            <div className="row">
                <img className="logo_home" src={Logo} srcSet={`${Logo2x} 2x, ${Logo3x} 3x`} ></img>
            </div>
            <div className="row" >
                <h1 className="BEM-VINDO-AO-EMPRESA" > Bem Vindo Ao Empresas </h1>
            </div>
            <div className="row">
                <h2 className="Lorem-ipsum-dolor-si"> Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</h2>
            </div>

            <form onSubmit={handleLogin}>
                <div className="input-field ">
                    <img className="prefix" src={MailIcon2x} srcSet={`${MailIcon2x} 2x, ${MailIcon3x} 3x`} ></img>
                    <input className="indent" onChange={(e)=>setEmail(e.target.value)} placeholder="E-mail" />
                </div>
                <div className="input-field ">
                    <img className="prefix" src={LockIcon} srcSet={`${LockIcon2x} 2x, ${LockIcon3x} 3x`} ></img>
                    <input className="indent"  onChange={(e) => setPassword(e.target.value)} placeholder="Senha" />
                </div>
                <div className="col s12">
                    <button type="submit" className="btn teal accent-4 background">
                        Entrar
                </button>
                </div>
            </form>

        </div>
    )
}

export default Login