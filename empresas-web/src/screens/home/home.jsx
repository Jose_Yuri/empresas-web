import React, { useState, useContext } from 'react';
import "./home-style.css"
import axios from 'axios'
import Nabar from '../../Components/NavBar/navbar'
import SearchBar from '../../Components/SearchNavBar/searchbar'
import Card from '../../Components/Card/card'
import List from '../../Components/List/list'
import { EmpresasContext } from '../../EmpresasContext'
const Home = () => {
    const [control, setControl] = useState(false)
    const [contextValue, setValue] = useState([])
    const [filter,setFilter] = useState("")
    const empresasContext = useContext(EmpresasContext)
    const handleShow = async () => {
        setControl(true)
        const token = localStorage.getItem("access-token")
        const uid = localStorage.getItem("uid")
        const client = localStorage.getItem("client")
        const response = await axios.get('https://empresas.ioasys.com.br/api/v1/enterprises', { headers: { "access-token": token, "client": client, "uid": uid } })
        console.log(response.data.enterprises)
        setValue(response.data.enterprises)
    }
    return (
            <div className="_Home">
                <nav className="">
                    {!control ? <Nabar Control={handleShow} /> : <SearchBar filter={(value)=>setFilter(value)} />}
                </nav>
                <div>
                    {!control ? <h2 className="Clique-na-busca-para">  Clique na busca para iniciar. </h2> : <List list={contextValue} filter={filter} />}
                </div>
            </div>

    )
}

export default Home