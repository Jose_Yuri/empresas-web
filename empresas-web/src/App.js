import React from 'react';
import Login from './screens/login/login'
import Home from './screens/home/home'
import './App.css';
import Router from './Routes/router'


function App() {
  return (
   <Router/>
  );
}

export default App;
