import React, { useState } from 'react';
import './searchbar-style.css'
const searchbar = (props) =>{
    const {filter} = props
    return(
        <div className="nav-wrapper">
            <form>
                <div className="center" >
                    <input  onChange={(e)=>filter(e.target.value)}  placeholder="Pesquisa" />
                </div>
            </form>
      </div>
    )
}

export default searchbar