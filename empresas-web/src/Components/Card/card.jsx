import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import Empresa from '../../Assets/img-e-1-lista.png'
import "./card-style.css"

const Card = (props) => {
    const routeHook = useHistory();
    const {empresa} = props
    return (
        <div className="col s12 m7">
            <div className="card horizontal">
                <div className="card-image" > 
                    <img className="img" src={Empresa} />
                </div>
                <div className="">
                    <div onClick={()=>routeHook.push("/detalhe",{empresa:empresa})} className="card-content">
                        <p  className="Empresa1"> {empresa.enterprise_name} </p>
                        <p className="Negcio" > {empresa.enterprise_type.enterprise_type_name} </p>
                        <p className="Brasil">  {empresa.country} </p>
                    </div>
                    <div className="card-reveal">
                    </div>
                </div>
            </div>
        </div>
    )
}


export default Card