import React from 'react';
import Card from '../Card/card'
import EmpresaContext from '../../EmpresasContext'
import "./list-style.css"

const List = (props) => {
    const vetor = [1,2,3,4,5,6,7]
    const {list,filter} = props
    return (
        <div  className="list" >
            <div >
                 {(filter == '') ? list.map(elment => <Card empresa = {elment}/>) : 
                 list.filter(elment2 => elment2.enterprise_name.includes(filter) || elment2.enterprise_type.enterprise_type_name.includes(filter))
                 .map(result =>  <Card empresa={result} />)}
                {}
            </div>

        </div>
    )
}

export default List