import React from 'react'
import {BrowserRouter,Route,Switch, Redirect} from 'react-router-dom'

import Login from "../screens/login/login"
import Home from '../screens/home/home'
import Detalhe from '../screens/empresa-detalhe/empresa-detalhe'

const PrivateRoute = ({Component,Path}) =>{
    const Auth = localStorage.getItem('access-token')

    return(
        Auth ? <Route path={Path} component={Component}/> : <Route to ='/' />
    )
}

const Router = () =>{
    return(
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Login} />
                <Route  path="/detalhe" component={Detalhe} />
                <PrivateRoute Path='/home' Component={Home} />
            </Switch>
        </BrowserRouter>
    )
}

export default Router